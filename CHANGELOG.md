## 2.0.1 (2021-11-25)

### unexpected (1 change)

- [Test of new category for git trailer](martin.bocek/docs-gen-test@a0f0f61eca10d06ac971b4d3e2087f28cf677386)

## 2.0.0 (2021-11-22)

### added (1 change)

- [Major version update](martin.bocek/docs-gen-test@3bcdf8f014b9134594230869e38539c383ce8025)

## 1.1.0 (2021-11-22)

### changed (2 changes)

- [MR commit](martin.bocek/docs-gen-test@d258088bb835df8af6054b67924ae954f5cd4eaf)
- [Change of update.md](martin.bocek/docs-gen-test@ddc6ebef0795ffd19ff9c360407ba1116fc63158)

### deprecated (1 change)

- [Deprecated commit](martin.bocek/docs-gen-test@a89b88ced519981946da557e714f508cdad4f5a8)

### fixed (1 change)

- [Fix update.md](martin.bocek/docs-gen-test@2ee225beb3466a49bcb0905e8f23aeabe49f65af)

### added (1 change)

- [Update.md added](martin.bocek/docs-gen-test@bb7de42bd3c9f30d368798336326a890622e6520)

## 1.0.1 (2021-11-22)

### changed (2 changes)

- [MR commit](martin.bocek/docs-gen-test@d258088bb835df8af6054b67924ae954f5cd4eaf)
- [Change of update.md](martin.bocek/docs-gen-test@ddc6ebef0795ffd19ff9c360407ba1116fc63158)

### deprecated (1 change)

- [Deprecated commit](martin.bocek/docs-gen-test@a89b88ced519981946da557e714f508cdad4f5a8)

### fixed (1 change)

- [Fix update.md](martin.bocek/docs-gen-test@2ee225beb3466a49bcb0905e8f23aeabe49f65af)

### added (2 changes)

- [Update.md added](martin.bocek/docs-gen-test@bb7de42bd3c9f30d368798336326a890622e6520)
- [Information about instalation added](martin.bocek/docs-gen-test@5e84aef609caf411dba3288524c97c4651082b35)
