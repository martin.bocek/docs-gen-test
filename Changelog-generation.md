# Changelog generation

### Description
This document describes steps to implement automatic generation of changelog based on versions of releases. Result of this proces is changelog document containing changes in repository based on commit messages. 

## Requirements
- Commit messages with git trailers (keywords for generation)
- Git tags using semantic versions
- Calling Gitlab API for generation

## Commit messages
Commit message sould contain subject, description and git trailer. Also commit titles and descriptions should be understandable without context.

Template of commit message:
```
<Commit message subject>

<Commit message description>

Changelog: feature
```
Changelog entries are grouped by git trailer, default is `Changelog`. Commits without trailers are not shown in result changelog.

Changelog trailer can have for example values: `added`, `fixed`, `changed`, `deprecated`, `removed`, `security`, `performance`, `other`.

Commit message also can contain other trailers e.g. `Services` with values `banks`, `budgets`, ... 

More detail information on [GitLab Docs](https://docs.gitlab.com/ee/development/changelog.html)

## Semantic versioning
Changelog generation works only if git tags are following [semantic versioning](https://semver.org/).

Example:
```
v1.0.0-pre1
v1.0.0
v1.1.0
v2.0.0
```

## Generating changelog with API
Changelog is generated with http request to the GitLab API.

#### API URL
```
// returns generated changelog data
GET /projects/:id/repository/changelog

// commits generated changelog to repository
POST /projects/:id/repository/changelog 
```

More information about supported atributes are on [GitLab Docs](https://docs.gitlab.com/ee/api/repositories.html#add-changelog-data-to-a-changelog-file)

Values for example commands:
- Project ID: ID of repository
- Location: Where GitLab is hosted
- API token: Access token to repository (category API)

Request example using `curl`:
```
curl --request POST --header "PRIVATE-TOKEN: token" --data "version=1.0.0" "https://gitlab.com/api/v4/projects/42/repository/changelog"
```
